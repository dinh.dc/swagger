FROM python:3.9-alpine
LABEL author="aino"

WORKDIR /app

ARG CI_COMMIT_AUTHOR=$CI_COMMIT_AUTHOR
ENV CI_COMMIT_AUTHOR=${CI_COMMIT_AUTHOR}

ARG CI_COMMIT_SHA=$CI_COMMIT_SHA
ENV CI_COMMIT_SHA=${CI_COMMIT_SHA}

ARG CI_COMMIT_TIMESTAMP=$CI_COMMIT_TIMESTAMP
ENV CI_COMMIT_TIMESTAMP=${CI_COMMIT_TIMESTAMP}

COPY . /app

RUN pip install -r /app/requirements.txt
RUN python /app/manage.py collectstatic --no-input
RUN python /app/manage.py migrate

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

